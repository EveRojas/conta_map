import React from 'react';
//import Axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFolderMinus } from '@fortawesome/free-solid-svg-icons'






class AdminReports extends React.Component{



   render() {
console.log(this.props.reports)
// this.state.reports.map((reports, index) => {
//          ele.complaint , ele.bias, ele.agressor, ele.acting, ele.info, ele.proof, ele.record_Location, ele.loc_victim, ele.ip, ele.date }) 

    return (
        <div className= 'container-3table'>
        <div>
          <div className= 'table-header'>  
             <h7><b>REPORTS</b></h7>
             </div>      
         
          <div className='tables'>
            <table>
             <tr>

               <th>COMPLAINT</th>
               <th>BIAS</th>
               <th>AGRESSOR</th>
               <th>ACTION</th>
               <th>ID</th>
               <th>REMOVE</th> 
             </tr>

             
             { this.props.reports 
             	? this.props.reports.map(ele=> (
                    <tr>
                      <td>{ele.complaint}</td>
                      <td>{ele.bias}</td>
                      <td>{ele.agressor}</td>
                      <td>{ele.acting}</td>
                      <td>{ele._id}</td>
                      <td><FontAwesomeIcon style={{padding: '2px 20px 0px 20px'}} icon={faFolderMinus} size="1x"/></td>

                   </tr>
             	))
               : null
             }

            </table>
          </div>
        </div>

        <div>
          <div className= 'table-header'>  
             <h7><b>DISCRIPTION</b></h7>
          </div>      
          
          <div className='tables'>
            <table>
             <tr>
               <th>DETAILS</th>
               <th>EVIDENCE</th>
               <th>ID</th>
               <th>REMOVE</th>
             </tr>
            
                 { this.props.reports 
             	? this.props.reports.map(ele=> (
               <tr>
               <td>{ele.info}</td>
               <td>{ele.proof.length > 0 ? 'true' : 'false'}</td>
               <td>{ele._id}</td>
               <td><FontAwesomeIcon  style={{padding: '2px 20px 0px 20px'}}  icon={faFolderMinus} size="1x"/></td>
              

             </tr>
               ))
               : null
             }
            

            </table>
          </div>
        </div>

        <div>
          <div className= 'table-header'>  
             <h7><b>RADAR</b></h7>
          </div>      
          <div className='tables'>
            <table> 
             <tr>
               <th>COUNTRY</th>
               <th>USER IP</th>
               <th>CREATED AT</th>               
               <th>ID</th>  
               <th>REMOVE</th>
             </tr>

             { this.props.reports 
             	? this.props.reports.map(ele=> ( 
             <tr>
               <td>{ele.record_Location}</td>
               <td>{ele.Ip}</td>
               <td>{ele.createdAt}</td>
               <td>{ele._id}</td>
               <td> <FontAwesomeIcon style={{padding: '1px 20px 1px 20px'}} icon={faFolderMinus} size="1x"/> </td>
             </tr>
                           ))
               : null
             } 
            </table>
         </div>
        </div>
      </div>    
     )
  } 
}
 
export default AdminReports

