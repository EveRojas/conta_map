import React , { useState } from 'react';
import Axios from 'axios';
import {din_url} from '../config.js'

const Login = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		password : '',
		hidden:true
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
          const response = await Axios.post(`${din_url}/users/login`,{
          	email:form.email,
          	password:form.password
          })
          setMessage(response.data.message)
          if( response.data.ok ){
              localStorage.setItem('token',JSON.stringify(response.data.token)) 
              setTimeout( ()=> {props.change_log_status(true);props.history.push('/users/admin')},2000)    
          }
          
		}
        catch(error){
        	console.log(error)
        }
	}

	return <form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='container-start'>
	         <p><b>ENTER</b></p>
	         <label>Email</label>    
		     <input className='static-log' name="email"/>
		     <label>Password</label>
		     <input className='static-log' type="password" name="password"/>
		     <button className='static-login'>Login</button>
		     <div style={{color:'red'}}><p>{message}</p></div>
	       </form>
}

export default Login
