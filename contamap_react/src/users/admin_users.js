import React from 'react';
//import Axios from 'axios' 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserSlash } from '@fortawesome/free-solid-svg-icons'
import { faUserEdit } from '@fortawesome/free-solid-svg-icons'


class AdminUsers extends React.Component {

// state = { //state is by default an object
        
//         users: [
//             { name: '',  email: '', remove:'', update:'', access:'total'}, // total can modify data
//             { name: '',  email: '', remove: '', update:'', access:'restrict'},// restrict can't modify
//          ]
//         }
   
// const table_headings=['name', 'email', 'remove', 'update', 'access']
// const table_data = [{name}, {email}]


   render() {

      return (
         <div className= 'container'> 
         <div className= 'table-header'>  
         <h7><b>MANAGE USERS</b></h7>
         </div>      
         <div className='container-table'>
          <table> 
            <tr>
               <th>ID</th>
               <th>NAME</th>
               <th>EMAIL</th>
               <th>REMOVE</th>
               <th>UPDATE</th>
               <th>ACCESS</th>
            </tr>
            
            <tr>
               <td>1</td>
               <td>Eve</td>
               <td>eve@email</td>
             <td><FontAwesomeIcon style={{padding: '3px 20px 0px 20px'}} icon={faUserSlash} size="1x"/> </td>
               <td><FontAwesomeIcon style={{padding: '3px 20px 0px 20px'}} icon={faUserEdit} size="1x"/></td>
               <td> restrict </td>
           </tr>
           <tr>
               <td>2</td>
               <td>Eve</td>
               <td>eveline@email</td>
               <td> <FontAwesomeIcon style={{padding: '3px 20px 0px 20px'}} icon={faUserSlash} size="1x"/> </td>
               <td> <FontAwesomeIcon style={{padding: '3px 20px 0px 20px'}} icon={faUserEdit} size="1x"/> </td>
               <td> full </td>
           </tr>
           

          </table> 
        </div>
      </div>      
     )
  }
} 

export default AdminUsers

