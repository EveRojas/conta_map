import React from "react"
import { BrowserRouter as Router, Route } from "react-router-dom";
import axios from 'axios';
import {din_url} from './config.js'
import Home from "./scenes_home/home";
import Contact from "./scenes_home/contact_us";
import Navbar from "./scenes_home/components/navbar";
import Footer from "./scenes_home/components/footer";
import Terms from "./scenes_home/components/terms";
import Report from "./scenes_report/Report";
import Search from "./scenes_search/search";
import ShowReport from "./scenes_report/show_report";
import Numbers from "./scenes_search/numbers";
import {coords} from './country_coords';
import './index.css';
import OsceData from "./scenes_search/osce_data";
import Login from "./users/Login";
//import Register from "./users/Register"
import Admin from "./users/admin";
import AdminReports from "./users/admin_reports";
import AdminUsers from "./users/admin_users";


// import ReportsCharts from "./users/admin_reports_charts"



class App extends React.Component {
state = {
	reports : [],
  stats: [],
  count:0,
  isAdmin: false
}
//========================================================================================
//========================================================================================
    componentDidMount() {
      console.log('coords', coords)
       this.getReports(coords)
    }
//========================================================================================
//========================================================================================
    getOsce = (stats,reports) => {
      // console.log(OsceData)
      OsceData.forEach( ele => {
        //let years_count = 0
        const index = stats.findIndex( obj => obj.name === ele.name)
        if(index !== -1){
          let total = 0
          ele.number_per_year.forEach( ele2 => {
             for( var year in ele2 ){
                  total += ele2[year]
             }
          })
        stats[index].osceData = total
        }

      })
      // console.log('stats=====>', stats)
      this.setState({stats,reports})

    }
//========================================================================================
//========================================================================================
    getReports = async (coords) => {
    	let url =`${din_url}/report/getall`;
              const stats=[]
        
	    try {
	      const res = await axios.get(url);
const reports = res.data
      
        
        res.data.forEach( ele => {

          const rec_loc_idx = coords.findIndex( loc => ele.record_Location === loc.name)
          const stats_idx = stats.findIndex( loc2 => loc2.name === ele.record_Location)
          
         
          if( stats_idx === -1 ){
var obj = {
                name: ele.record_Location,
                latd: coords[rec_loc_idx]['latd'],
                long: coords[rec_loc_idx]['long'],
                count: 1,
                complaint:{[ele.complaint]:1},
                bias:{[ele.bias]:1},
                _id: ele._id
              }
        
              stats.push(obj)

              
          }else{

            if(ele.complaint in stats[stats_idx].complaint){
              stats[stats_idx].complaint[ele.complaint] = stats[stats_idx].complaint[ele.complaint] + 1
            } else {
              stats[stats_idx].complaint[ele.complaint] = 1
            }

            if(ele.bias in stats[stats_idx].bias){
              stats[stats_idx].bias[ele.bias] = stats[stats_idx].bias[ele.bias] + 1
            } else {
              stats[stats_idx].bias[ele.bias] = 1
            }

            stats[stats_idx].count = stats[stats_idx].count + 1
          }
          
        })
        console.log("stats=>",stats)

         this.getOsce(stats,reports)
         
	    
	        } catch (error) {
            console.log('error', error)
	      this.setState({error:'something went wrong'})      
	    }
    }

//////////////////////////////////////////////////////////////////////////
    change_log_status = (status) => this.setState({isAdmin:status})       


    render(){

    return (
      <Router>
        <div>  
         <Navbar/>
          <Route exact path="/"      component={Home} />
          <Route path="/report" component={Report} />
          <Route path="/show_report" render={props => <ShowReport {...props} report={this.state.report}/>} />
          <Route path="/search" render={props => <Search {...props} stats={this.state.stats}/>} />
          <Route path="/numbers/:_id" render={props => <Numbers {...props} stats={this.state.stats}/>} />
          <Route path="/users/login" render={props => <Login {...props} change_log_status={this.change_log_status}/>} />
          <Route path="/users/admin" component={Admin}/>
          <Route path="/users/admin_users" component={AdminUsers}/>
          <Route path="/users/admin_reports" render={props=> <AdminReports reports={this.state.reports}/>}/>
          {/*<Route path="/users/register" component={Register} />*/}
          <Route path="/contact_us" component={Contact}/> 
          <Route path="/terms" component={Terms}/> 
          <div>
          <Footer/>
          </div>
        </div>
      </Router>
   
    )
   }
} 
export default App

    


    // this.setState({report:res.data},()=>{
            //        this.state.report.forEach( item => {
            //        const location_index = this.state.stats.findIndex( ele => ele.report === item.record_Location )
            //        const  country_index = Coords.findIndex( ele2 => ele2.name === item.record_Location )
            //        const  lat = Coords.findIndex( ele2 => ele2.name === ele2.lat )
            //        const  lng = Coords.findIndex( ele2 => ele2.name === ele2.lng )  
            //        console.log("== index ==>",country_index)
            //                       if(location_index === -1){
            //            this.setState({stats:[...this.state.stats,{
            //                          record_Location:item.record_Location,
            //                          count:1,
            //                          lat:Coords[country_index].lat,
            //                          lng:Coords[country_index].lng
            //                }]
            //                     })
            //        }else{
            //           const { stats } = this.state
            //           stats[location_index].count = stats[location_index].count + 1
            //           this.setState({stats})
            //        }