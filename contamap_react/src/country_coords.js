 const coords=[
   //1
  {
        
        "latd":12.5,
        "long":-69.96666666,
        "name": "Aruba",  
        "capital": "Oranjestad"
    }, 
  //2
  {
        
        "latd":33,
        "long":65, 
        "name": "Afghanistan",  
        "capital": "Kabul"
    }, 
  //3
  {
        
        "latd":-12.5, 
        "long":18.5, 
        "name": "Angola",  
        "capital": "Luanda"
    }, 
  //4
  {
        
        "latd":18.25,
        "long":-63.16666666,              
        "name": "Anguilla",  
        "capital": "The Valley"
    }, 
  //5
  {
        
        "latd":60.116667,
        "long":19.9, 
        "name": "Aland Islands",  
        "capital": "Mariehamn"
    }, 
  //6
  {
        
        "latd":41,
        "long":20,
        "name": "Albania",  
        "capital": "Tirana"
    }, 
  //7
  {
        
        "latd":42.5,
        "long":1.5, 
        "name": "Andorra",  
        "capital": "Andorra la Vella"
    }, 
  //8
  {
        
        "latd":24,
        "long":54, 
        "name": "United Arab Emirates",  
        "capital": "Abu Dhabi"
    }, 
  //9
  {
        
        "latd":-34,
        "long":-64, 
        "name": "Argentina",  
        "capital": "Buenos Aires"
    }, 
  //10
  {
        
        "latd":-14.33333333,
        "long":-170,
        "name": "American Samoa",  
        "capital": "Pago Pago"
    }, 
  //11
  {
        "latd":-90,
        "long":0,
        "name": "Antarctica",  
        "capital": null
    }, 
    //12
    {
        
        "latd":-49.25,
        "long":69.167,
        "name": "French Southern and Antarctic Lands",  
        "capital": "Port-aux-Fran\u00e7ais"
    }, 
  //13
  {
        
        "latd":17.05,
        "long":-61.8,
        "name": "Antigua and Barbuda",  
        "capital": "Saint John's"
    }, 
  //14
  {
         
        "latd":-27,
        "long":133, 
        "name": "Australia",  
        "capital": "Canberra"
    }, 
  //15
  {
        
        "latd":47.33333333,
        "long":13.33333333,
        "name": "Austria",  
        "capital": "Vienna"
    }, 
  //16
  {
        
        "latd":40.5,
        "long":47.5,
        "name": "Azerbaijan",  
        "capital": "Baku"
    }, 
  //17
  {
        
        "latd":-3.5,
        "long":30,
        "name": "Burundi",  
        "capital": "Bujumbura"
    }, 
  //18
  {
        
        "latd":50.83333333,
        "long":4,
        "name": "Belgium",  
        "capital": "Brussels"
    }, 
  //19
  {
        
        "latd":9.5,
        "long":2.25, 
        "name": "Benin",  
        "capital": "Porto-Novo"
    }, 
  //20
  {
        
        "latd":13,
        "long": -2, 
        "name": "Burkina Faso",  
        "capital": "Ouagadougou"
    }, 
  //21
  {
        
        "latd":24,
        "long":90,
        "name": "Bangladesh",  
        "capital": "Dhaka"
    }, 
  //22
  {
        
        "latd":43,
        "long":25,
        "name": "Bulgaria",  
        "capital": "Sofia"
    }, 
  //23
  {
        
        "latd":26,
        "long":50.55,
        "name": "Bahrain",  
        "capital": "Manama"
    }, 
  //24
  {
        
        "latd":24.25,
        "long":-76,
        "name": "Bahamas",  
        "capital": "Nassau"
    }, 
  //25
  {
        
        "latd":44,
        "long":18, 
        "name": "Bosnia and Herzegovina",  
        "capital": "Sarajevo"
    }, 
  //26
  {
        
        "latd":18.5,
        "long":-63.41666666,            
        "name": "Saint Barthélemy",  
        "capital": "Gustavia"
    }, 
  //27
  {
        
        "latd":53,
        "long":28, 
        "name": "Belarus",  
        "capital": "Minsk"
    }, 
  //28
  {
        
        "latd":17.25,
        "long":-88.75,
        "name": "Belize",  
        "capital": "Belmopan"
    }, 
  //29
  {
        
        "latd":32.33333333,
        "long":-64.75,
        "name": "Bermuda",  
        "capital": "Hamilton"
    }, 
  //30
  {
        
        "latd":-17, 
        "long":-65,
        "name": "Bolivia",  
        "capital": "Sucre"
    }, 
  //31
  {
        
        "latd":13.16666666,
        "long":-59.53333333, 
        "name": "Barbados",  
        "capital": "Bridgetown"
    }, 
  //32
  {
        
        "latd":4.5,
        "long":114.66666666,
        "name": "Brunei",  
        "capital": "Bandar Seri Begawan"
    }, 
  //34
  {
        
        "latd":27.5, 
        "long":90.5,
        "name": "Bhutan",  
        "capital": "Thimphu"
    }, 
  //35
  {
        
        "latd":-54.43333333,
        "long":3.4,
        "name": "Bouvet Island",  
        "capital": null
    }, 
  //36
  {
        
        "latd":-22,
        "long":24,
        "name": "Botswana",  
        "capital": "Gaborone"
    }, 
  //37
  {
        
        "latd":7, 
        "long":21,
        "name": "Central African Republic",  
        "capital": "Bangui"
    }, 
  //38
  {
        "latd":60,
        "long":-95,
        "name": "Canada",  
        "capital": "Ottawa"
    },
  //39 
    {
        
        "latd":-12.5,
        "long":96.83333333, 
        "name": "Cocos (Keeling) Islands",  
        "capital": "West Island"
    }, 
  //40
  {
        
        "latd":47,
        "long":8,
        "name": "Switzerland",  
        "capital": "Bern"
    }, 
  //41
  {
        "latd":-30,
        "long":-71, 
        "name": "Chile",  
        "capital": "Santiago"
    },
  //42 
    {
        "latd":35, 
        "long":105,
        "name": "China",  
        "capital": "Beijing"
    }, 
  //43
    {
        
        "latd":8,
        "long":-5, 
        "name": "Ivory Coast",  
        "capital": "Yamoussoukro"
    }, 
  //44
  {
        
        "latd":6, 
        "long":12,
        "name": "Cameroon",  
        "capital": "Yaounde"
    }, 
  //45
  {
        "latd":0,
        "long":25,
        "name": "DR Congo",  
        "capital": "Kinshasa"
    },
  //46 
    {
        
        "latd":-1,
        "long":15,
        "name": "Republic of the Congo",  
        "capital": "Brazzaville"
    }, 
  //47
  {
        
        "latd":-21.23333333,
        "long":-159.76666666,
        "name": "Cook Islands",  
        "capital": "Avarua"
    }, 
  //48
  {
        
        "latd":4,
        "long":-72,
        "name": "Colombia",  
        "capital": "Bogota"
    }, 
  //49
  {
        
        "latd":-12.16666666,
        "long":44.25, 
        "name": "Comoros",  
        "capital": "Moroni"
    }, 
  //50
  {
        
        "latd":16,
        "long":-24,
        "name": "Cape Verde",  
        "capital": "Praia"
    }, 
  //51
  {
        
        "latd":10,
        "long":-84,
        "name": "Costa Rica",  
        "capital": "San José"
    }, 
  //52
  {
        
        "latd":21.5,
        "long":-80, 
        "name": "Cuba",  
        "capital": "Havana"
    }, 
  //53
  {
        
        "latd":12.116667,
        "long":-68.933333,
        "name": "Curaçao",  
        "capital": "Willemstad"
    }, 
  //54
  {
        
        "latd":-10.5,
        "long":105.66666666,
        "name": "Christmas Island",  
        "capital": "Flying Fish Cove"
    }, 
  //55
  {
        
        "latd":19.5,
        "long":-80.5 ,
        "name": "Cayman Islands",  
        "capital": "George Town"
    }, 
  //56
  {
        
        "latd":35,
        "long":33 ,
        "name": "Cyprus",  
        "capital": "Nicosia"
    }, 
  //57
  {
        
        "latd":49.75,
        "long":15.5,
        "name": "Czech Republic",  
        "capital": "Prague"
    }, 
  //58
  {
        "latd":51,
        "long":9,
        "name": "Germany",  
        "capital": "Berlin"
    },
  //59 
    {
        
        "latd":11.5,
        "long":43 ,
        "name": "Djibouti",  
        "capital": "Djibouti"
    }, 
  //60
  {
        
        "latd":15.41666666,
        "long":-61.33333333 ,
        "name": "Dominica",  
        "capital": "Roseau"
    }, 
  //61
  {
        
        "latd":56,
        "long":10 ,
        "name": "Denmark",  
        "capital": "Copenhagen"
    }, 
  //62
  {
        
        "latd":19,
        "long":-70.66666666 ,
        "name": "Dominican Republic",  
        "capital": "Santo Domingo"
    }, 
  //63
  {
        
        "latd":28,
        "long": 3 ,
        "name": "Algeria",  
        "capital": "Algiers"
    }, 
  //64
  {
        "latd":-2,
        "long":-77.5,
        "name": "Ecuador",  
        "capital": "Quito"
    },
  //65 
    {
        
        "latd":27,
        "long":30,
        "name": "Egypt",  
        "capital": "Cairo"
    }, 
  //66
  {
        
        "latd":15,
        "long":39,
        "name": "Eritrea",  
        "capital": "Asmara"
    }, 
  //67
  {
        
        "latd":24.5,
        "long":-13,
        "name": "Western Sahara",  
        "capital": "Laayoune"
    }, 
  //68
  {
        "latd":40,
        "long":-4,
        "name": "Spain",  
        "capital": "Madrid"
    }, 
  //69 
    {
        
        "latd":59,
        "long":26,
        "name": "Estonia",  
        "capital": "Tallinn"
    }, 
  //70
  {
        
        "latd":8,
        "long":38,
        "name": "Ethiopia",  
        "capital": "Addis Ababa"
    }, 
  //71
  {
        
        "latd":64,
        "long":26,
        "name": "Finland",  
        "capital": "Helsinki"
    }, 
  //72
  {
        
        "latd":-18,
        "long":175,
        "name": "Fiji",  
        "capital": "Suva"
    }, 
  //73
  {
        
        "latd":-51.75,
        "long":-59,
        "name": "Falkland Islands",  
        "capital": "Stanley"
    }, 
  //74
  {
        
        "latd":46,
        "long":2,
        "name": "France",  
        "capital": "Paris"
    }, 
  //75
  {
        
        "latd":62,
        "long":-7,
        "name": "Faroe Islands",  
        "capital": "T\u00f3rshavn"
    }, 
  //76
  {
        "latd":6.91666666,
        "long":158.25,
        "name": "Micronesia",  
        "capital": "Palikir"
    },
  //77 
    {
        
        "latd":-1,
        "long":11.75,
        "name": "Gabon",  
        "capital": "Libreville"
    }, 
  //78
  {
        
        "latd":54,
        "long":-2,
        "name": "UK",  
        "capital": "London"
    }, 
  //79
  {
        
        "latd":42,
        "long":43.5,
        "name": "Georgia",  
        "capital": "Tbilisi"
    }, 
  //80
  {
        
        "latd":49.46666666,
        "long":-2.58333333,
        "name": "Guernsey",  
        "capital": "St. Peter Port"
    }, 
  //81
  {
        
        "latd":8,
        "long":-2,
        "name": "Ghana",  
        "capital": "Accra"
    }, 
  //82
  {
        
        "latd":36.13333333,
        "long":-5.35 ,
        "name": "Gibraltar",  
        "capital": "Gibraltar"
    }, 
  //83
  {
        
        "latd":11, 
        "long":-10 ,
        "name": "Guinea",  
        "capital": "Conakry"
    }, 
  //84
  {
        
        "latd":16.25,
        "long":-61.583333 ,
        "name": "Guadeloupe",  
        "capital": "Basse-Terre"
    }, 
  //85
  {
        
        "latd":13.46666666,
        "long":-16.56666666,
        "name": "Gambia",  
        "capital": "Banjul"
    }, 
  //86
  {
        
        "latd":12,
        "long":-15,
        "name": "Guinea-Bissau",  
        "capital": "Bissau"
    }, 
  //87
  {
        
        "latd":2,
        "long":10,
        "name": "Equatorial Guinea",  
        "capital": "Malabo"
    }, 
  //88 
  {
        
        "latd":39,
        "long":22,
        "name": "Greece",  
        "capital": "Athens"
    }, 
  //89
  {
        
        "latd":12.11666666,
        "long":-61.66666666,
        "name": "Grenada",  
        "capital": "St. George's"
    }, 
  //90
  {
        "latd":72, 
        "long":-40,
        "name": "Greenland",  
        "capital": "Nuuk"
    },
  //91 
    {
        
        "latd":15.5,
        "long":-90.25,
        "name": "Guatemala",  
        "capital": "Guatemala City"
    }, 
  //92
  {
        
        "latd":4,
        "long":-53 ,
        "name": "French Guiana",  
        "capital": "Cayenne"
    }, 
  //93
  {
        
        "latd":13.46666666,
        "long":144.78333333,
        "name": "Guam",  
        "capital": "Guancheng Hui District of Zhengzhou"
    }, 
  //94
  {
        
        "latd":5,
        "long":-59,
        "name": "Guyana",  
        "capital": "Georgetown"
    }, 
  //95
  {
        
        "latd":22.267,
        "long":114.188,
        "name": "Hong Kong",  
        "capital": "City of Victoria"
    },
  //96
  {
        
        "latd":15,
        "long":-86.5,
        "name": "Honduras",  
        "capital": "Tegucigalpa"
    }, 
  //97
  {
        
        "latd":45.16666666,
        "long":15.5 ,
        "name": "Croatia",  
        "capital": "Zagreb"
    }, 
  //98
  {
        
        "latd":19,
        "long":-72.41666666,
        "name": "Haiti",  
        "capital": "Port-au-Prince"
    }, 
  //99
  {
        
        "latd":47,
        "long":20 ,
        "name": "Hungary",  
        "capital": "Budapest"
    }, 
  //100
  {
        "latd":-5,
        "long":120,
        "name": "Indonesia",  
        "capital": "Jakarta"
    }, 
  //101
    {
        
        "latd":54.25,
        "long":-4.5,
        "name": "Isle of Man",  
        "capital": "Douglas"
    }, 
  //102
  {
        
        "latd":20,
        "long":77,
        "name": "India",  
        "capital": "New Delhi"
    }, 
  //103
  {
        
        "latd":-6,
        "long":71.5,
        "name": "British Indian Ocean Territory",  
        "capital": "Diego Garcia"
    }, 
  //104
  {
        
        "latd":53,
        "long":-8,
        "name": "Ireland",  
        "capital": "Dublin"
    }, 
  //105
  {
        
        "latd":32,
        "long":53,
        "name": "Iran",  
        "capital": "Tehran"
    }, 
  //106
  {
        
        "latd":33,
        "long":44 ,
        "name": "Iraq",  
        "capital": "Baghdad"
    }, 
  //107
  {
        
        "latd":65,
        "long":-18,
        "name": "Iceland",  
        "capital": "Reykjavik"
    }, 
  //108
  {
        
        "latd":31.47,
        "long":35.13,
        "name": "Israel",  
        "capital": "Jerusalem"
    }, 
  //109
  {
        
        "latd":42.83333333,
        "long":12.83333333,
        "name": "Italy",  
        "capital": "Rome"
    }, 
  //110
  {
        
        "latd":18.25,
        "long":-77.5,
        "name": "Jamaica",  
        "capital": "Kingston"
    }, 
  //111
  {
        
        "latd":49.25,
        "long":-2.16666666,
        "name": "Jersey",  
        "capital": "Saint Helier"
    }, 
  //112
  {
        
        "latd":31,
        "long":36,
        "name": "Jordan",  
        "capital": "Amman"
    }, 
  //113
  {
        
        "latd":36,
        "long":138,
        "name": "Japan",  
        "capital": "Tokyo"
    }, 
  //114
  {
        "latd":48,
        "long":68,
        "name": "Kazakhstan",  
        "capital": "Astana"
    }, 
  //115  
    {
        
        "latd":1,
        "long":38,
        "name": "Kenya",  
        "capital": "Nairobi"
    }, 
  //116
  {
        
        "latd":41,
        "long":75,
        "name": "Kyrgyzstan",  
        "capital": "Bishkek"
    }, 
  //117
  {
        
        "latd":13,
        "long":105,
        "name": "Cambodia",  
        "capital": "Phnom Penh"
    }, 
  //118
  {
        "latd":1.41666666,
        "long":173,
        "name": "Kiribati",  
        "capital": "South Tarawa"
    }, 
  //119  
    {
        
        "latd":17.33333333,
        "long":-62.75,
        "name": "Saint Kitts and Nevis",  
        "capital": "Basseterre"
    }, 
  //120
  {
        
        "latd":37,
        "long":127.5,
        "name": "South Korea",  
        "capital": "Seoul"
    }, 
  //121
  {
        "latd":42.666667,
        lgn: 21.166667,
        "name": "Kosovo",  
        "capital": "Pristina"
    }, 
  //122  
    {
        
        "latd":29.5,
        "long":45.75,
        "name": "Kuwait",  
        "capital": "Kuwait City"
    }, 
  //123
  {
        
        "latd":18, 
        "long":105,
        "name": "Laos",  
        "capital": "Vientiane"
    }, 
  //124
  {
        
        "latd":33.83333333,
        "long":35.83333333,
        "name": "Lebanon",  
        "capital": "Beirut"
    }, 
  //125
  {
        
        "latd":6.5,
        "long":-9.5 ,
        "name": "Liberia",  
        "capital": "Monrovia"
    }, 
  //126
  {
        
        "latd":25,
        "long": 17,
        "name": "Libya",  
        "capital": "Tripoli"
    }, 
  //127
  {
        
        "latd":13.88333333,
        "long":-60.96666666,
        "name": "Saint Lucia",  
        "capital": "Castries"
    }, 
  //128
  {
        
        "latd":47.26666666,
        "long":9.53333333,
        "name": "Liechtenstein",  
        "capital": "Vaduz"
    }, 
  //129
  {
        
        "latd":7, 
        "long":81,
        "name": "Sri Lanka",  
        "capital": "Colombo"
    }, 
  //130
  {
        
        "latd":-29.5,
        "long":28.5,
        "name": "Lesotho",  
        "capital": "Maseru"
    }, 
  //131
  {
        
        "latd":56, 
        "long":24,
        "name": "Lithuania",  
        "capital": "Vilnius"
    }, 
  //132  
  {
        
        "latd":49.75,
        "long":6.16666666,
        "name": "Luxembourg",  
        "capital": "Luxembourg"
    }, 
  //133
  {
        
        "latd":57,
        "long":25,
        "name": "Latvia",  
        "capital": "Riga"
    }, 
  //134
  {
        
        "latd":22.16666666,
        "long":113.55 ,
        "name": "Macau",  
        "capital": null
    }, 
  //135
  {
        
        "latd":18.08333333,
        "long":-63.95,
        "name": "Saint Martin",  
        "capital": "Marigot"
    }, 
  //136
  {
        
        "latd":32,
        "long":-5,
        "name": "Morocco",  
        "capital": "Rabat"
    }, 
  //137
  {
        
        "latd":43.73333333,
        "long":7.4,
        "name": "Monaco",  
        "capital": "Monaco"
    }, 
  //138
  {
        
        "latd":47,
        "long":29,
        "name": "Moldova",  
        "capital": "Chisinau"
    }, 
  //139
  {
        
        "latd":-20,
        "long":47,
        "name": "Madagascar",  
        "capital": "Antananarivo"
    }, 
  //140
  {
        
        "latd":3.25,
        "long":73,
        "name": "Maldives",  
        "capital": "Malé"
    }, 
  //141
  {
        "latd":23,
        "long":-102,
        "name": "Mexico",  
        "capital": "Mexico City"
    }, 
  //142
    {
         "latd":9,
         "long":168,
        "name": "Marshall Islands",  
        "capital": "Majuro"
    }, 
  //143  
    {
        
        "latd":41.83333333,
        "long":22,
        "name": "Macedonia",  
        "capital": "Skopje"
    }, 
  //144
  {
        
        "latd":17,
        "long":-4,
        "name": "Mali",  
        "capital": "Bamako"
    }, 
  //145
  {
        
        "latd":35.83333333,
        "long":14.58333333,
        "name": "Malta",  
        "capital": "Valletta"
    }, 
  //146
  {
        
        "latd":22,
        "long":98,
        "name": "Myanmar",  
        "capital": "Naypyidaw"
    }, 
  //147
  {
        
        "latd":42.5,
        "long":19.3,
        "name": "Montenegro",  
        "capital": "Podgorica"
    }, 
  //148
  {
        "latd":46, 
        "long":105,
        "name": "Mongolia",  
        "capital": "Ulan Bator"
    },
  //149   
    {
        
        "latd":15.2,
        "long":145.75,
        "name": "Northern Mariana Islands",  
        "capital": "Saipan"
    }, 
  //150
  {
        
        "latd":-18.25,
        "long":35,
        "name": "Mozambique",  
        "capital": "Maputo"
    }, 
  //151
  {
        
        "latd":20,
        "long":-12,
        "name": "Mauritania",  
        "capital": "Nouakchott"
    }, 
  //152
  {
        
        "latd":16.75,
        "long":-62.2,
        "name": "Montserrat",  
        "capital": "Plymouth"
    }, 
  //153
  {
        
        "latd":14.666667,
        "long":-61,
        "name": "Martinique",  
        "capital": "Fort-de-France"
    }, 
  //154
  {
        
        "latd":-20.28333333,
        "long":57.55,
        "name": "Mauritius",  
        "capital": "Port Louis"
    }, 
  //155
  {
        
        "latd":-13.5,
        "long":34,
        "name": "Malawi",  
        "capital": "Lilongwe"
    }, 
  //156
  {
        "latd":2.5,
        "long":112.5,
        "name": "Malaysia",  
        "capital": "Kuala Lumpur"
    }, 
  //157  
    {
        
        "latd":-12.83333333,
        "long":45.16666666,
        "name": "Mayotte",  
        "capital": "Mamoudzou"
    }, 
  //158
  {
        
        "latd":-22,
        "long":17,
        "name": "Namibia",  
        "capital": "Windhoek"
    }, 
  //159
  {
        
        "latd":-21.5,
        "long":165.5,
        "name": "New Caledonia",  
        "capital": "Noum\u00e9a"
    }, 
  //160
  {
        
        "latd":16,
        "long":8,
        "name": "Niger",  
        "capital": "Niamey"
    }, 
  //161
  {
        
        "latd":-29.03333333,
        "long":167.95 ,
        "name": "Norfolk Island",  
        "capital": "Kingston"
    }, 
  //162
  {
        
        "latd":10, 
        "long":8,
        "name": "Nigeria",  
        "capital": "Abuja"
    }, 
  //163
  {
        
        "latd":13,
        "long":-85 ,
        "name": "Nicaragua",  
        "capital": "Managua"
    }, 
  //164
  {
        
        "latd":-19.03333333,
        "long":-169.86666666 ,
        "name": "Niue",  
        "capital": "Alofi"
    }, 
  //165
  {
        
        "latd":52.5, 
        "long":5.75,
        "name": "Netherlands",  
        "capital": "Amsterdam"
    }, 
  //166
  {
        
        "latd":62, 
        "long":10,
        "name": "Norway",  
        "capital": "Oslo"
    }, 
  //167
  {
        
        "latd":28,
        "long":84,
        "name": "Nepal",  
        "capital": "Kathmandu"
    }, 
  //168
  {
        
        "latd":-0.53333333,
        "long":166.91666666,
        "name": "Nauru",  
        "capital": "Yaren"
    }, 
  //169
  {
        "latd":-41,
        "long": 174 ,
        "name": "New Zealand",  
        "capital": "Wellington"
    }, 
  //170  
    {
        
        "latd":21,
        "long":57,
        "name": "Oman",  
        "capital": "Muscat"
    }, 
  //171
  {
        
        "latd":30,
        "long":70,
        "name": "Pakistan",  
        "capital": "Islamabad"
    }, 
  //172
  {
        
        "latd":9,
        "long":-80,
        "name": "Panama",  
        "capital": "Panama City"
    }, 
  //173
  {
        
        "latd":-25.06666666,
        "long":-130.1,
        "name": "Pitcairn Islands",  
        "capital": "Adamstown"
    }, 
  //174
  {
        
        "latd":-10,
        "long":-76,
        "name": "Peru",  
        "capital": "Lima"
    }, 
  //175
  {
        
        "latd":13,
        "long":122,
        "name": "Philippines",  
        "capital": "Manila"
    }, 
  //176
  {
        
        "latd":7.5,
        "long":134.5,
        "name": "Palau",  
        "capital": "Ngerulmud"
    }, 
  //177
  {
        "latd":-6,
        "long":147,
        "name": "Papua New Guinea",  
        "capital": "Port Moresby"
    }, 
  //178  
    {
        
        "latd":52,
        "long":20,
        "name": "Poland",  
        "capital": "Warsaw"
    }, 
  //179
  {
        
        "latd":18.25,
        "long":-66.5,
        "name": "Puerto Rico",  
        "capital": "San Juan"
    }, 
  //180
  {
        
        "latd":40,
        "long":127,
        "name": "North Korea",  
        "capital": "Pyongyang"
    }, 
  //181
  {
        "latd":39.5,
        "long":-8,
        "name": "Portugal",  
        "capital": "Lisbon"
    }, 
  //182  
    {
        
        "latd":-23,
        "long":-58,
        "name": "Paraguay",  
        "capital": "Asunci\u00f3n"
    }, 
  //183 
  {
        "latd":31.9,
        "long":35.2,
        "name": "Palestine",  
        "capital": "Ramallah"
    }, 
  //184  
    {
        "latd":-15,
        "long":-140,
        "name": "French Polynesia",  
        "capital": " Pape'ete"
    }, 
  //185  
    {
        
        "latd":25.5,
        "long":51.25,
        "name": "Qatar",  
        "capital": "Doha"
    }, 
  //186
  {
        
        "latd":-21.15,
        "long":55.5,
        "name": "Réunion",  
        "capital": "Saint-Denis"
    }, 
  //187
  {
        
        "latd":46, 
        "long":25,
        "name": "Romania",  
        "capital": "Bucharest"
    }, 
  //188
  {
        "latd":60,
        "long":100,
        "name": "Russia",  
        "capital": "Moscow"
    }, 
  //189  
    {
        
        "latd":-2,
        "long":30,
        "name": "Rwanda",  
        "capital": "Kigali"
    }, 
  //190
  {
        
        "latd":25,
        "long":45,
        "name": "Saudi Arabia",  
        "capital": "Riyadh"
    }, 
  //191
  {
        
        "latd":15,
        "long":30,
        "name": "Sudan",  
        "capital": "Khartoum"
    }, 
  //192
  {
        
        "latd":14,
        "long":-14,
        "name": "Senegal",  
        "capital": "Dakar"
    }, 
  //193
  {
        
        "latd":1.36666666, 
        "long":103.8,
        "name": "Singapore",  
        "capital": "Singapore"
    }, 
  //194
  {
        
        "latd":-54.5,
        "long":-37,
        "name": "South Georgia",  
        "capital": "King Edward Point"
    }, 
  //195
  {
        
        "latd":78,
        "long":20,
        "name": "Svalbard and Jan Mayen",  
        "capital": "Longyearbyen"
    }, 
  //196
  {
        
        "latd":-8,
        "long":159,
        "name": "Solomon Islands",  
        "capital": "Honiara"
    }, 
  //197
  {
        
        "latd":8.5,
        "long":-11.5,
        "name": "Sierra Leone",  
        "capital": "Freetown"
    }, 
  //198
  {
        
        "latd":13.83333333,
        "long":-88.91666666,
        "name": "El Salvador",  
        "capital": "San Salvador"
    }, 
  //199
  {
        
        "latd":43.76666666,
        "long":12.41666666,
        "name": "San Marino",  
        "capital": "City of San Marino"
    }, 
  //200
  {
        
        "latd":10, 
        "long":49,
        "name": "Somalia",  
        "capital": "Mogadishu"
    }, 
  //201
  {
        
        "latd":46.83333333,
        "long":-56.33333333 ,
        "name": "Saint Pierre and Miquelon",  
        "capital": "Saint-Pierre"
    }, 
  //202
  {
        
        "latd":44, 
        "long":21 ,
        "name": "Serbia",  
        "capital": "Belgrade"
    }, 
  //203
  {
        
        "latd":7, 
        "long":30 ,
        "name": "South Sudan",  
        "capital": "Juba"
    }, 
  //204
  {
        
        "latd":1, 
        "long":7,
        "name": "São Tomé and Príncipe,",  
        "capital": "Sao Tome"
    }, 
  //205  
  {
        
        "latd":4,
        "long":-56,
        "name": "Suriname",  
        "capital": "Paramaribo"
    }, 
  //206
  {
        
        "latd":48.66666666,
        "long":19.5,
        "name": "Slovakia",  
        "capital": "Bratislava"
    }, 
  //207
  {
        
        "latd":46.11666666,
        "long":14.81666666,
        "name": "Slovenia",  
        "capital": "Ljubljana"
    }, 
  //208
  {
        
        "latd":62,
        "long":15,
        "name": "Sweden",  
        "capital": "Stockholm"
    }, 
  //209
  {
        
        "latd":-26.5,
        "long":31.5,
        "name": "Swaziland",  
        "capital": "Lobamba"
    }, 
  //210
  {
        
        "latd":18.033333,
        "long":-63.05,
        "name": "Sint Maarten",  
        "capital": "Philipsburg"
    }, 
  //211
  {
        
        "latd":-4.58333333,
        "long":55.66666666,
        "name": "Seychelles",  
        "capital": "Victoria"
    }, 
  //212
  {
        
        "latd":35,
        "long":38,
        "name": "Syria",  
        "capital": "Damascus"
    }, 
  //213
  {
        
        "latd":21.75,
        "long":-71.58333333,
        "name": "Turks and Caicos Islands",  
        "capital": "Cockburn Town"
    }, 
  //214
  {
        
        "latd":15, 
        "long": 19,
        "name": "Chad",  
        "capital": "N'Djamena"
    }, 
  //215
  {
        
        "latd":8, 
        "long":1.16666666,
        "name": "Togo",  
        "capital": "Lome"
    }, 
  //216
  {
        
        "latd":15, 
        "long":100,
        "name": "Thailand",  
        "capital": "Bangkok"
    }, 
  //217
  {
        
        "latd":39,
        "long":71,
        "name": "Tajikistan",  
        "capital": "Dushanbe"
    }, 
  //218
  {
        
        "latd":-9,
        "long":-172,
        "name": "Tokelau",  
        "capital": "Fakaofo"
    }, 
  //219
  {
        
        "latd":40,
        "long":60,
        "name": "Turkmenistan",  
        "capital": "Ashgabat"
    }, 
  //220
  {
        
        "latd":-8.83333333,
        "long":125.91666666,
        "name": "Timor-Leste",  
        "capital": "Dili"
    }, 
  //221
  {
        
        "latd":-20,
        "long":-175,
        "name": "Tonga",  
        "capital": "Nuku'alofa"
    }, 
  //222
  {
        
        "latd":11,
        "long":-61,
        "name": "Trinidad and Tobago",  
        "capital": "Port of Spain"
    }, 
  //223
  {
        
        "latd":34,
        "long":9,
        "name": "Tunisia",  
        "capital": "Tunis"
    }, 
  //224
  {
        
        "latd":39,
        "long":35,
        "name": "Turkey",  
        "capital": "Ankara"
    }, 
  //225
  {
        
        "latd":-8,
        "long": 178,
        "name": "Tuvalu",  
        "capital": "Funafuti"
    }, 
  //226
  {
        
        "latd":23.5,
        "long":121,
        "name": "Taiwan",  
        "capital": "Taipei"
    }, 
  //227
  {
        
        "latd":-6,
        "long":35,
        "name": "Tanzania",  
        "capital": "Dodoma"
    }, 
  //228
  {
        
        "latd":1,
        "long":32,
        "name": "Uganda",  
        "capital": "Kampala"
    }, 
  //229
  {
        "latd":49,
        "long":32,
        "name": "Ukraine",  
        "capital": "Kiev"
    },
  //230 
    {
        "latd":19.2911437,
        "long":166.618332,
        "name": "United States Minor Outlying Islands",  
        "capital": null
    }, 
  //231
    {
        
        "latd":-33, 
        "long":-56,
        "name": "Uruguay",  
        "capital": "Montevideo"
    }, 
  //232
  {
        "latd":38,
        "long":-97,
        "name": "USA",  
        "capital": "Washington D.C."
    }, 
  //233
    {
        "latd":41,
        "long":64,
        "name": "Uzbekistan",  
        "capital": "Tashkent"
    }, 
  //234  
    {
        
        "latd":41.9,
        "long":12.45,
        "name": "Vatican City",  
        "capital": "Vatican City"
    }, 
  //235
  {
        
        "latd":13.25,
        "long":-61.2,
        "name": "Saint Vincent and the Grenadines",  
        "capital": "Kingstown"
    }, 
  //236
  {
        
        "latd":8,
        "long":-66,
        "name": "Venezuela",  
        "capital": "Caracas"
    }, 
  //237
  {
        
        "latd":18.431383,
        "long":-64.62305,
        "name": "British Virgin Islands",  
        "capital": "Road Town"
    }, 
  //238
  {
        
        "latd":18.35,
        "long":-64.933333 ,
        "name": "United States Virgin Islands",  
        "capital": "Charlotte Amalie"
    }, 
  //239
  {
        
        "latd":16.16666666,
        "long":107.83333333,
        "name": "Vietnam",  
        "capital": "Hanoi"
    }, 
  //240
  {
        
        "latd":-16,
        "long":167,
        "name": "Vanuatu",  
        "capital": "Port Vila"
    }, 
  //241
  {
        
        "latd":-13.3,
        "long":-176.2,
        "name": "Wallis and Futuna",  
        "capital": "Mata-Utu"
    }, 
  //242
  {
        
        "latd":-13.58333333,
        "long":-172.33333333,
        "name": "Samoa",  
        "capital": "Apia"
    }, 
  //243
  {
        
        "latd":15, 
        "long":48,
        "name": "Yemen",  
        "capital": "Sana'a"
    }, 
  //244
  {
        
        "latd":-29,
        "long":24,
        "name": "South Africa",  
        "capital": "Pretoria"
    }, 
  //245
  {
        
        "latd":-15,
        "long":30,
        "name": "Zambia",  
        "capital": "Lusaka"
    }, 
  //246
  {
        
        "latd":-20,
        "long":30,
        "name": "Zimbabwe",  
        "capital": "Harare"
    },
     {
        "latd":40.0691,
        "long":45.0382,     
        "name": "Armenia",  
        "capital":"Yerevan"
    }
  


]
export { coords }
















  // {
  //       "latd":37.0902,
  //       "long":-95.7129,
  //       "name": "USA",  
  //       "capital": "Washington D.C."
  //   },
  //     {
  //       "latd":40.4637,
  //       "long":3.7492,
  //       "name": "Spain",  
  //       "capital": "Madrid"
  //   },
  //    { 
  //       "latd":51.9194,
  //       "long":19.1451,
  //       "name": "Poland",  
  //       "capital": "Warsaw"
  //   },  
  //    {
        
  //       "latd":31,
  //       "long":36,
  //       "name": "Jordan",  
  //       "capital": "Amman"
  //   }
  //   ] 