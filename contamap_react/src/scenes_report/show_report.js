import React from "react"
//import axios from 'axios';
//import ReactDOM from "react-dom";
//import Report from './Report'

class showReport extends React.Component {

	componentDidMount(){
		console.log(this.props)
	}


render(){
return(
  
  <div className='container-show'>
  
        <h1>Your Report</h1>
        <p>*this report has no legal value*</p>
        <div>
        <p>Sent at:{this.props.location.state.date}</p>
       	<p>Complaint: {this.props.location.state.complaint}</p>
       	<p>On ground: {this.props.location.state.bias}</p>
        <p>Perpetrated by: {this.props.location.state.agressor}</p>
        <p>Acting: {this.props.location.state.acting}</p>
        <p>Where it happened:{this.props.location.state.record_Location}</p>
        <p>What happened: {this.props.location.state.info}</p>
        </div>
        <div>Evidence attached:{this.props.location.state.proof.map((el)=><h5>{el.photo_url}</h5>)}</div>
        <button className='send-button'><a href={"mailto:" + this.props.email}>Send it</a></button>
        <button className='gomap-button' onClick={() => this.props.history.push({pathname:"/search"})}>Go to Map</button>
      

  </div>
  )
 }
}
export default showReport



