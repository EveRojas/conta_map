import React from "react"
import axios from 'axios';
import {din_url} from '../config.js'
//import ReactDOM from "react-dom";
//import change from './change.jpg';
import UploadImages from '../helpers_cloud/Uploadimages';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons'



class Report extends React.Component{

  state ={
    complaint:'',
    bias:'',
    agressor:'',
    acting:'',
    info:'',
    proof:[],
    display: false,
    Ip:'banana',
    record_Location:{}, 
    loc_victim:{},
    user_id:'',
    error:'',
    date: new Date().toDateString(),
    }

  componentDidMount(){
    navigator.geolocation.getCurrentPosition( position => {
      const coords = {
            lat:position.coords.latitude,
            lng:position.coords.longitude
          }
        this.setState({loc_victim:coords},()=>console.log('=== coords ===>',this.state.loc_victim))
    })
  }

  handleChange = (event) => {
     //console.log(event.target.name,event.target.value)
    this.setState({[event.target.name]: event.target.value});
  }

  handleSubmit = async (event) => {
    event.preventDefault();

    let {complaint, bias, agressor, acting, info, record_Location} = this.state
    if(complaint==='' ||bias===''||agressor===''|| acting===''|| info===''|| record_Location==={}){
      return alert('Please fill the items!')
      //complaint, bias, agressor, acting, info, proof, record_Location, Ip, loc_victim 
    }
    let url = `${din_url}/report/create`;
    try {
      let {complaint, bias, agressor, acting, info, proof, record_Location, Ip, loc_victim} = this.state
      const res = await axios.post(url,{complaint, bias, agressor, acting, info, proof, record_Location, Ip, loc_victim});
      console.log('res from create===>',res);
          alert('Your complain was submited!')
          this.props.history.push({
               pathname:"/show_report",
               state: {...this.state}
           })
    } catch (error) {
      this.setState({error:'something went wrong'})      
    }
}

  getAddress = async (ad) => {
    console.log(ad)
     const country = ad.terms[ad.terms.length-1].value
     //console.log(country)
     //const point_of_interest= ad.terms.shift().value
     //console.log(point_of_interest)
     this.setState({record_Location:country})
 
}

  setProof = (el) => {
      const { proof } = this.state
      proof.push(el)
      this.setState({proof:proof,display:true})
      setTimeout(()=>this.setState({display:false}),5000)
          
     // handleClick =()=>{this.state}
     //onClick={this.handleClick}
  }

  // destroyProof=(public_id)=>{

    // window.cloudinary.uploader.destroy(public_id,function(result) { console.log(result) });
                     // }  
//window.cloudinary.uploader.destroy
  render() {
    //     let {complaint, bias, agressor, acting, info, proof, record_Location, Ip, loc_victim, error } = this.state;
    return ( 
    <div className='container'>
    <div className='report-box'>
    <h1>REPORT</h1>
    </div>
    <div className='grid-form'>
    
    <div className='box-form'>           
     
     <form onSubmit={this.handleSubmit}>        
        <label>
          COMPLAINT:
          <select className='drop-button' name='complaint' value={this.state.value} onChange={this.handleChange}>
            <option> Select...</option>
            <option value="Discrimination">Discrimination</option>
            <option value="Intimidation">Intimidation</option>
            <option value="Harassment">Harassment</option>
            <option value="Physical Violence">Physical Violence</option>
            <option value="Property Damage">Property Damage</option>
            <option value="Censorship">Censorship</option>
            <option value="Prohibition of Protest">Physical Violence</option>
            <option value="Other Complaint">Other</option>
          </select>
        </label>

        <label>
          ON GROUND OF:
        <select className='drop-button' name='bias'value={this.state.value} onChange={this.handleChange}>
             <option> Select...</option>
             <option value="Sex, Gender, Sexuality">Sex, Gender, Sexuality</option>
             <option value="Faith, Beliefs">Faith, Beliefs</option>
             <option value="Ethnicity, Race, Nationality">Ethnicity, Race, Nationality</option>
             <option value="Political Opnions, Affiliation">Political Opnions, Affiliation</option>
             <option value="Social Status">Social Status</option>
             <option value="Disability">Disability</option>
             <option value="Age,Physical Features">Age, Physical Features</option>
             <option value="Other Bias">Other</option>
           </select>
         </label>

         <label>
           PERPETRATED BY:
           <select required className='drop-button' name='agressor'value={this.state.value} onChange={this.handleChange}>
             <option> Select...</option>
             <option value="Commom Citizen">Commom Citizen</option>
             <option value="Public Agent">Public Agent</option>
             <option value="Private Agent">Private Agent</option>
             <option value="Other Perpetrator">Other</option>
           </select>
         </label>

         <label>
           ACTING:
           <select className='drop-button' name='acting' value={this.state.value} onChange={this.handleChange}>
              <option> Select...</option>
              <option value="Alone">Alone</option>
              <option value="in Group">In Group</option>
              <option value="Other">Other</option>
           </select>
         </label>
         <label>
           TELL US: 
           <textarea className='text-box' maxLength="240" name='info' placeholder='What happened?' onChange={this.handleChange}></textarea>
         </label>
         <label >
           WHERE IT HAPPENED: 
              <GooglePlacesAutocomplete
                  className='text-box' onSelect={(ad)=>this.getAddress(ad)}
              />
         </label>
         <button className='submit-button'><p><b>SUBMIT</b></p></button>
         </form>

      </div>
<div className='box-proof'>
     <label>
           EVIDENCE:
         <UploadImages className='evidence-button'  setProof={this.setProof}/>
              {this.state.display 
      ? <h5>File uploaded!</h5>
      : null 
     }
     {
      this.state.proof.map((el)=>  <div>
       <img style={{height:'5vh', width:'5%' }} src={el.photo_url} alt='cloudinary'/>
   <FontAwesomeIcon onClick={()=>this.destroyProof(el.public_id)} icon={faTrash} size="1x"/>       
   </div>)
     }
     
     </label> 
     </div>
   </div> 
 </div>     
    )
   }
 }

export default Report 

// const styles = {
//   marginTop: "20%",
//   textAlign: "center"
// };

 //      <FontAwesomeIcon onClick={()=>this.destroyProof(el.public_id)} icon={faTrash} size="1x"/>