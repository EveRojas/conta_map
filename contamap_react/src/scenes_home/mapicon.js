import React from "react";
//import { library } from '@fortawesome/fontawesome-svg-core';
import { faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'



export default function MapIcon(){
  return(
    <div className='icon-container'>
    <a href='./report'
       className="map-icon">
       <FontAwesomeIcon icon={faMapMarkerAlt} size="3x"/>
    </a>
    </div>
  );
}
