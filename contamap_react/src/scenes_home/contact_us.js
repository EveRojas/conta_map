import React from "react";
import axios from 'axios';
//import Side from './contact.jpg';
import {din_url} from '../config.js'


class Contact extends React.Component {

state = {}

    handleChange=(event)=>{

    	this.setState({[event.target.name]: event.target.value})

    }

	handleSubmit = async (event) => {
    event.preventDefault();

   
	  let { name , email , subject , message_content } = this.state
	  // console.log('state ====>', this.state)
	  let url = `${din_url}/emails/send_email`;
      
      try{
      	   const res = await axios.post(url,{name, email, subject, message_content}); 
      	   console.log('res mail==>',res);
      	   alert('email sent!')

       
    } catch (error) {
      alert({error:'something went wrong'})      
    }
}

render(){

return (
	<div className='container'>
	<div className='report-box'>
    <h1>CONTACT US</h1>
    </div>
	<div className='contact-box'>
	<form onSubmit={this.handleSubmit} onChange= {this.handleChange}>
	 <label>
	 Name:
	 <input name='name' className= "contact-input"></input>
	 </label>

	 <label>
	 Email:
	 <input name= 'email' className="contact-input"></input>
	 </label>
	 
	 <label>
	 Subject:
	 <input name= 'subject' className="contact-input"></input>
	 </label>  

     <label>
	 Message:
	 <input  name = 'message_content' className= "contact-input"></input>
	 </label>
     <label>
	 <button className="contact-button">Send</button>  
     </label>
	</form>

	</div>
	</div>
 )
 }
}
export default Contact

// const styles = {
//   marginTop: "20%",
//   textAlign: "center"
//  };


	    // <img className="contact-img" style= {{'width':'50%'}}src={Side} alt="contact img"/>