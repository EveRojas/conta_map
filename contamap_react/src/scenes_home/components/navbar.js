import React from "react";
import { NavLink } from "react-router-dom";
import logo from './logo_noback_small.png';
import SocialFollow from './socialmedia.js';

const Navbar = () => {
  return (

    <header>
    <nav>
    <img style={{width:'50px'}} src={logo} alt="small logo"/>
    <label className="intro-mark">
    <b>CONTA MAP</b>
    The Map of Intolerance
    </label>
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/"}
      >
        Home 
      </NavLink>
    
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/report"}
      >
      <b>REPORT</b>
      </NavLink>

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/search"}
      >
        Search
      </NavLink>
      <SocialFollow/>
    
    </nav>
    </header>
    
  );
};

export default Navbar;

const styles = {
  active: {
    color: "orange"
  },
  default: {
    textDecoration: "none",
    color: "black"
  }
};

