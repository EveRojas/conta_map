import React from "react";
import LogoPartner from './logo_partner.jpg';
import LogoSupporter from './logo_supporter.jpg';
import logo from './logo_noback_small.png';
import { NavLink } from "react-router-dom";

export class Footer extends React.Component {

	render() {
    return (
     
      <footer>
      <div className='foot-note'>
      <img style={{width:'60%'}}src={logo} alt="small logo"/>
      </div>
      
       <div>
       <div>
       <b><span>Get</span>Together:</b>
       </div>
       <div> 
        <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/users/login"}
      >
        Admin
      </NavLink>
      </div>
      <div>
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/"}
      >
        Donate
      </NavLink>
      </div>
      <div>
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/contact_us"}
      >
        Contact Us
      </NavLink>
      </div>
      <div>
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/terms"}
      >
      Privacy & Terms
      </NavLink>
      </div>

       </div>

       <div>
       <b><span>Our</span>Partners:</b>
       <div>
       <p><img src={LogoPartner} alt='logo partners'/></p>
       <p><img src={LogoPartner} alt='logo partners'/></p>
       </div>

       </div>
       
       <div>
       <b><span>Our</span>Supporters:</b> 
       <div>
       <p><img src={LogoSupporter} alt='logo supporters'/></p>
       <p><img src={LogoSupporter} alt='logo supporters'/></p>
       
       </div>
       </div>

      
      </footer>
    );
  }
}
  export default Footer


const styles = {

  active: {
    color: "orange"
  },
  default: {
    textDecoration: "none",
    color: "black",
  }
};

       //<img style={{width:'50px'}} src={logo}/>
       

  //contamap © 2019