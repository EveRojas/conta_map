import React from "react";
//import axios from "axios";
//import Search from "./search";
//import { library } from '@fortawesome/fontawesome-svg-core';
import {faFileImport} from '@fortawesome/free-solid-svg-icons';
import { faChartBar} from '@fortawesome/free-solid-svg-icons';
import {faChartPie}from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class Numbers extends React.Component {
 state = {
 	complaint_array : [],
 	bias_array:[],
 	name_array:[],
 	osceData_array:[]

 }

 componentDidMount(){
 	if( this.props.stats.length > 0){

      // console.log('props=>',this.props.stats)

	 	const { _id } = this.props.match.params
	 	//console.log(this.props.match.params)
	 	const index = this.props.stats.findIndex( ele => ele._id === _id)
	 	
	 	const complaint_array = Object.entries(this.props.stats[index].complaint)
	      
	 	this.setState({complaint_array})
 	}

    if( this.props.stats.length > 0){

      // console.log('props=>',this.props.stats)

	 	const { _id } = this.props.match.params
	 	//console.log(this.props.match.params)
	 	const index = this.props.stats.findIndex( ele => ele._id === _id)
	 	
	 	const bias_array = Object.entries(this.props.stats[index].bias)    
	 	this.setState({bias_array})
 	} 	

 	  if( this.props.stats.length > 0){

      // console.log('props=>',this.props.stats)

	 	const { _id } = this.props.match.params
	 	//console.log(this.props.match.params)
	    const index = this.props.stats.findIndex( ele => ele._id === _id)
	 
	 	//const name_array = Object.values(this.props.stats[index].name)
	 	//console.log('name===',name_array) 
	 	this.setState({name_array:this.props.stats[index].name})

	 	// const name_array = this.props.stats.map( (ele => ele === ele.name)
   //      this.setState({name_array})
            // console.log({name_array})
 }

  if( this.props.stats.length > 0){

      // console.log('props=>',this.props.stats)

	 	const { _id } = this.props.match.params
	 	//console.log(this.props.match.params)
	 	const index = this.props.stats.findIndex( ele => ele._id === _id)
	 	
	 	this.setState({osceData_array:this.props.stats[index].osceData})
 	}

//  componentDidMount(){
//  	const complaint = []
//  	this.setState({complaint:this.ṕrops})
//  }

// render_complaint = () => {
// 	return this.props.stats.forEach((ele)=> {
//       for( var key in ele.complaint ){
//       	console.log(key,' = ',ele.complaint[key])
//           return <h1>{key} = {ele.complaint[key]}</h1>
//       }
//     })
 }

render(){

return(

	<div className='container-numbers'>
	  
	  <div className='box-country'>
       {
          //this.state.name_array.map( (ele) => {
             <h2><b>{this.state.name_array}</b></h2>
          //})
		} 
	  </div>
	  
	  <div className='grid-numbers'>
	  
	  <div className='grid-chart'>
	    <FontAwesomeIcon icon={faChartBar} size="5x"/>
	  </div>
	  <div>
	    {
          this.state.complaint_array.map( (ele,i) => {
             return <p key={i}><b>{ele[0]} <span>.... {ele[1]}</span></b></p>
          })
		}  
	  </div>	  			
	  </div>

	  <div className='grid-numbers-pie'>
      <div> 
       {
          this.state.bias_array.map( (ele,i) => {
             return <p key={i}><b> <span> {ele[1]} .... </span>{ele[0]}</b> </p>
          })
		}  
      </div>
      
      <div className='grid-chart-pie'>
      <FontAwesomeIcon icon={faChartPie} size="5x"/>  
      </div>  
 	  
 	  </div>
      

      <div className='grid-numbers'>
	  <div className='grid-chart'>
	    <FontAwesomeIcon icon={faFileImport} size="5x"/>
	  </div>
	  <div>
	    {
               <p><b>from OSCE <span>.... {this.state.osceData_array}</span></b></p> 
           
		}  
		    <a href={"http://hatecrime.osce.org/#participating-states"}>know more</a>

	  </div>	  			
	  </div>
                             
	</div>

        )  
  }
}
export default Numbers




