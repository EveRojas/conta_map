import React from "react";
//import { library } from '@fortawesome/fontawesome-svg-core';
import { faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
//import axios from 'axios';
//import Search from './search';
//import Coodrs from '../country_coords';


export default function PinPoint(props){

  console.log('==PROPS PINS ==>', props)
  return(

    <div onClick={()=>props.history.push({pathname:`/numbers/${props._id}`})} className='pin-container'>
    <div className="pin-counter">
    <h6>{props.count}</h6>
    </div>
    <div className="pin-icon">
    <FontAwesomeIcon icon={faMapMarkerAlt} size="3x"/>
    </div>
    </div>
  );
}


// <h6>{props.osceData}</h6>
// <h6>{props.count}