import React from 'react'
import widgetStyle from './widgetstyle';

export default class UploadImages extends React.Component{

	uploadWidget = () => {
        window.cloudinary.openUploadWidget({ 
        	cloud_name: 'dyfnrqoys', 
        	upload_preset: 'contamap', 
			tags:['user'],
			stylesheet:widgetStyle
            
        },
            (error, result)=> {
				//debugger
                if(error){
					alert(error)
                }else{               							  
				
                this.props.setProof({photo_url:result[0].secure_url,public_id:result[0].public_id})			  
                
                }
            });
    }


	render(){
		return (
					<button className ="static-button"
                    	onClick={this.uploadWidget} > Upload
                    </button>
                
		)
	}
}

