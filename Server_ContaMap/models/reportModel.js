const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reportSchema = new Schema({

	             complaint:{ 
                         type: String,
                         required:true, 
                         },

                 bias:{
                         type: String,
                         required:true, 
                        },

                 agressor:{
                       type: String,
                       required:true, 
                          },

                 acting:{
                         type:String,
                         required:true, 
                        },

                 info:{ 
                         type: String,
                         required:true,
                      },

                 proof:{ 
                         type: [],
                         required:true,
                      },

                 Ip:{
                 	     type:String,
                         required:true,
                       },
          
                record_Location: {
			                    type:Object, 
			                    required:true 
		               },

                loc_victim:{   
                                type: Object, 

		             },      
                       
                createdAt:{ 
                                type:Date,
                                default:Date.now,
                                required:true },

                user_id:{
                          type:String
                }
 
             
})
module.exports =  mongoose.model('report', reportSchema);


