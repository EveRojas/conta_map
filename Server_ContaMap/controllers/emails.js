const nodemailer = require('nodemailer')
// Import pwd form form mailer where we can hide our mailbox password
const config    = require('../config.js')

// selecting mail service and authorazing with our credentials
const transport = nodemailer.createTransport({
// you need to enable the less secure option on your gmail account
// https://myaccount.google.com/lesssecureapps?pli=1
	service: 'Gmail',
	auth: {
		user: config.user,
		pass: config.pass,
	}
});

const send_email = async (req,res) => {
	  const { name , email , subject , message_content } = req.body
	  console.log('contact ====>', name , email , subject , message_content)
	  const default_subject = 'This is a default subject'
	  const mailOptions = {
		    to: 'contanpo@gmail.com',
		    subject: "New message from " + name,
		    html: '<p>'+(subject || default_subject)+ '</p><p><pre>' + message_content + '</pre></p>'
	   }
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
           return res.json({on:true,message:'email sent'})
      }
      catch( err ){
           return res.json({ok:false,message:err})
      }
}

module.exports = { send_email }