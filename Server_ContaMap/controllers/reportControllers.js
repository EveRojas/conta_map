const Report = require('../models/reportModel.js') 
//const User   = require('../models/users.models'); 
const jwt    = require('jsonwebtoken');
const config = require('../config');
///////////////////////create////////////////////////

const create = async(req,res) => {
  const {complaint, bias, agressor, acting, info, proof, record_Location, Ip, loc_victim,user_id} = req.body
  console.log(complaint, bias, agressor, acting, info, proof, record_Location, Ip,user_id)
try{
     
     const response = await Report.create({complaint:complaint,
                                           bias:bias,
                                           agressor:agressor,
                                           acting:acting,
                                           info:info,
                                           proof:proof,
                                           Ip:Ip,
                                           record_Location:record_Location,
                                           loc_victim:loc_victim,
                                           user_id:user_id   
                                          })

     res.send(response)

     } 

catch(err){
	res.send(err)
 }

}

/////////////////////// remove ////////////////////////

const remove = async(req,res) => {
    
    const {_id} = req.body
console.log(req.body)
try{
    const response = await Report.deleteOne({_id:_id})
    res.send(response)
       } 

catch(err){
	res.send(err)
 }

}


/////////////////////// update////////////////////////

const update = async(req,res) => {
  const {complaint, bias, agressor, acting, info, proof, record_Location, Ip, loc_victim} = req.body

try{
     
     const response = await Report.updateOne({complaint:complaint,
                                           bias:bias,
                                           agressor:agressor,
                                           acting:acting,
                                           info:info,
                                           proof:proof,
                                           Ip:Ip,
                                           record_Location:record_Location,
                                           loc_victim:loc_victim
                                          })
                                        
     res.send(response)
     } 

catch(err){
	res.send(err)
 }

}

///////////////// get all /////////////////////////////

const getall =async(req,res)=>{
	 console.log('get all contr==========>')
	try{
		const response = await Report.find({})
    console.log(response)
    res.send(response)
	}
	catch(err){
		res.send(err)
	}
}
///////////////////////////////////////////////////////

const user_reports =async(req,res)=>{
 //console.log('server user controller ==========>')
const {token} =req.body
console.log('token from server ===========>', token)
       const decoded   = jwt.verify(token, config.secret, async(err,succ) => {
          if(err)return res.json({ok:false,message:'something went wrong'}) 
            try{
              const response = await Report.find({user_id:succ._id})
              console.log('Response ==>',response)
              res.send({ok:true,user_reports:response})
            }
            catch(err){
              res.send(err)
            }
       }); 
}



/////////////////get one /////////////////////////////

const getone =async(req,res)=>{

  const {_id} =req.params

  try{
    const response = await Report.findOne({_id:_id})
  }
  catch(err){
    res.send(err)
  }
}

module.exports ={

create,
remove,
update,
getall,
getone,
user_reports

}
