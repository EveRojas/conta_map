const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    config = require('./config');

const path = require('path')
const port = process.env.PORT || 9000;

// =================== initial settings ===================

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo
app.use(cors())
// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect(`mongodb+srv://contamap:${config.mlab_pass}@cluster0-1dnsn.mongodb.net/contamapdb?retryWrites=true&w=majority`, { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}

connecting()

mongoose.set('useCreateIndex', true);

app.use('/report',require('./routes/reportRouter'));

app.use('/users', require('./routes/users.routes'));

app.use('/emails', require('./routes/emails.js'))

app.listen(9000, () => console.log(`listening on port 9000`))