const router= require('express').Router()

const controller = require('../controllers/reportControllers.js')

router.post('/create', controller.create)
router.post('/remove', controller.remove)
router.post('/update', controller.update)
router.get('/getall', controller.getall)
router.get('/getone/:_id', controller.getone)
router.post('/user_reports', controller.user_reports)



module.exports = router
